{
	
	"intro section title" : "Room to Grow",

	"Intro section copy" : "Business has always thrived in Gaston County—from our hardworking textile mill heritage to our current roster of global manufacturers. We are county of makers and doers, with a passion for craft demonstrated in yesteryear’s textile mills and today’s thriving independent artisans. Close proximity to Charlotte’s international airport, location directly on the I-85 corridor and plenty of available land make Gaston an ideal location for companies, while a skilled and ready workforce, economic incentives and vibrant quality of life make it easy for businesses of all kinds to succeed here.",

	"Green Callout Line" : "From Dole Foods to National Gypsum, Bridgestone/Firestone to Parkdale, global companies are growing in Gaston County.",

	"background callout line" : "WHERE WILL YOUR ADVENTURE TAKE YOU?"
}