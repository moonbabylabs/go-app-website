import {Component, AfterViewInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

declare var jQuery: any;
declare var WOW: any;
declare var Slick: any;
declare var aboutContent: any;
declare var baseUrl: any;

@Component({
    selector: 'about-page',
    templateUrl: 'app/about-updated.html',
    providers: [ Title ]

})


export class AboutComponent implements AfterViewInit {

	content: any;

	baseUrl: any;

	constructor( private titleService: Title ) {

		titleService.setTitle('GastonOutside - About');

		this.content = aboutContent;

		this.baseUrl = baseUrl;

	}

	ngAfterViewInit() {

		jQuery(document).ready(function() {

			new WOW().init();


			/*  Slick Slider Active //testimonail
			============================================= */
			jQuery('.te-testimonial-wrap').slick({
				dots: false,
				autoplay: true,
				infinite: true,
				nextArrow: 'span.next',
				prevArrow: 'span.prev',
				speed: 1200,
				slidesToShow: 1,
				slidesToScroll: 1
			});

		});

	}

 }
