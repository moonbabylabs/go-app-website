"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var RecreationComponent = (function () {
    function RecreationComponent(titleService) {
        this.titleService = titleService;
        titleService.setTitle('GastonOutside - Recreation');
        this.activeTitle = 'Crowder\'s Mountain';
        this.activeParagraph = 'Famous for its panoramic view, rugged peaks and challenging hikes, this 5,096-acre state park is an adventure you can’t miss.';
        this.activeLinkText = 'Visit Crowder\'s Mountain >';
        this.activeLinkUrl = 'http://www.ncparks.gov/crowders-mountain-state-park ';
        this.activeAddress = '522 Park Office Ln, Kings Mountain, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.fifthhoverrec').addClass('active');
    }
    RecreationComponent.prototype.firstOnClick = function () {
        this.activeTitle = 'Brevard Station';
        this.activeParagraph = 'Stanley’s charming museum of local history will immerse you in the town’s past, providing a tour of farm life, the textile industry, the advent of the railroad and more.';
        this.activeLinkText = 'Visit Brevard Station >';
        this.activeLinkUrl = 'http://www.brevardstation.com/';
        this.activeAddress = '112 S Main St, Stanley, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.firstthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.secondOnClick = function () {
        this.activeTitle = 'C. Grier Truck Museum';
        this.activeParagraph = 'Housed in the original gas station where Carolina Freight had its humble beginnings, you can explore more than 7500 square feet of vintage trucking memorabilia spanning seven decades.';
        this.activeLinkText = 'Visit C. Grier Truck Museum >';
        this.activeLinkUrl = 'http://www.beamtruckmuseum.com/';
        this.activeAddress = '111 N Mountain St, Cherryville, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.secondhoverrec').addClass('active');
    };
    RecreationComponent.prototype.fourthOnClick = function () {
        this.activeTitle = 'Cherryville Heritage Park';
        this.activeParagraph = 'Fun and educational for the whole family, this downtown park traces the historic development of Cherryville through key buildings, including the first City Hall.';
        this.activeLinkText = 'Visit Cherryville Heritage Park >';
        this.activeLinkUrl = 'http://www.visitgaston.org/gaston-county-attractions/item/cherryville-heritage-park.html';
        this.activeAddress = '104 S. Jacob Street Cherryville, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.fourthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.fifthOnClick = function () {
        this.activeTitle = 'Crowder\'s Mountain';
        this.activeParagraph = 'Famous for its panoramic view, rugged peaks and challenging hikes, this 5,096-acre state park is an adventure you can’t miss.';
        this.activeLinkText = 'Visit Crowder\'s Mountain >';
        this.activeLinkUrl = 'http://www.ncparks.gov/crowders-mountain-state-park ';
        this.activeAddress = '522 Park Office Ln, Kings Mountain, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.fifthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.sixthOnClick = function () {
        this.activeTitle = 'Daniel Stowe Botanical Garden';
        this.activeParagraph = 'Called “one of the most beautiful gardens in the world,” this 110-acre garden features rolling woodlands, meadows and manicured flowers for hours of exploring.';
        this.activeLinkText = 'Visit Daniel Stowe Botanical Garden >';
        this.activeLinkUrl = 'http://www.dsbg.org/ ';
        this.activeAddress = '6500 S New Hope Rd, Belmont, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.sixthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.seventhOnClick = function () {
        this.activeTitle = 'Gaston County Museum';
        this.activeParagraph = 'The exhibits in this regional history museum rotate frequently, giving visitors plenty of reasons to return. Don’t miss the state’s largest collection of horse-drawn vehicles!';
        this.activeLinkText = 'Visit Gaston County Museum >';
        this.activeLinkUrl = 'http://www.gastoncountymuseum.org/';
        this.activeAddress = '131 W Main St, Dallas, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.seventhhoverrec').addClass('active');
    };
    RecreationComponent.prototype.eigthOnClick = function () {
        this.activeTitle = 'George Poston Park';
        this.activeParagraph = '100-acre park located in the heart of Gaston County with over 7 miles of nature and hiking trails, mountain bike trails, fishing lake and more. ';
        this.activeLinkText = 'Visit George Poston Park >';
        this.activeLinkUrl = 'http://www.gastongov.com/departments/parks-recreation/parks/george-poston-park';
        this.activeAddress = '1101 Lowell Spencer Mountain Rd, Gastonia, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.eigthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.ninthOnClick = function () {
        this.activeTitle = 'Schiele Museum';
        this.activeParagraph = 'This museum of natural history features the largest collection of land mammals in the Southeast, along with five permanent galleries, a planetarium, an Indian village and an 18th-century farm.';
        this.activeLinkText = 'Visit Schiele Museum >';
        this.activeLinkUrl = 'http://www.schielemuseum.org/ ';
        this.activeAddress = '1500 E Garrison Blvd, Gastonia, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.ninthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.tenthOnClick = function () {
        this.activeTitle = 'South Fork River Park';
        this.activeParagraph = '44-acre park with natural surface trails for walking and running, an access area for canoeing and kayaking, fishing, picnicking and over 800 feet of river frontage along the South Fork River.';
        this.activeLinkText = 'Visit South Fork River Park >';
        this.activeLinkUrl = 'http://www.visitgaston.org/parks/item/south-fork-river-park.html ';
        this.activeAddress = '4185 Mountain View St, Gastonia, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.tenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.eleventhOnClick = function () {
        this.activeTitle = 'Tailrace Marina';
        this.activeParagraph = 'Conveniently located just 15 minutes from Charlotte on the Catawba River, this marina offers access and kayak rentals for peaceful, flat-water paddling on beautiful Lake Wylie.';
        this.activeLinkText = 'Visit Tailrace Marina >';
        this.activeLinkUrl = 'http://www.tailracemarina.com/ ';
        this.activeAddress = '1000 Marina Village Dr, Mt Holly, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.eleventhhoverrec').addClass('active');
    };
    RecreationComponent.prototype.twelthOnClick = function () {
        this.activeTitle = 'U.S. National White Water Center';
        this.activeParagraph = 'This high-energy outdoor recreation center features the world’s largest recirculating river for rafting and paddling, 14 miles of hiking, biking and running trails, and a vast outdoor climbing facility. ';
        this.activeLinkText = 'Visit U.S. National White Water Center >';
        this.activeLinkUrl = 'http://usnwc.org/ ';
        this.activeAddress = '5000 Whitewater Center Pkwy, Charlotte, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.twelthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.thirteenthOnClick = function () {
        this.activeTitle = 'Christmas Town USA';
        this.activeParagraph = 'Each December, hundreds of thousands of twinkling lights turn this small mill town into a spectacular holiday display. Visitors stroll down Main Street surrounded by the sights and sounds of Christmas.';
        this.activeLinkText = 'Visit Christmas Town USA >';
        this.activeLinkUrl = 'http://www.mcadenville-christmastown.com/';
        this.activeAddress = 'McAdenville, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.thirteenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.fourteenthOnClick = function () {
        this.activeTitle = 'Rankin Lake';
        this.activeParagraph = 'A community landmark since its construction in 1922, Rankin Lake is a favorite spot for family gatherings, picnicking, bank fishing, and other activities.';
        this.activeLinkText = 'Visit Rankin Lake >';
        this.activeLinkUrl = 'http://www.cityofgastonia.com/rankin-lake-park.html ';
        this.activeAddress = ' 1750 Rankin Lake Rd., Gastonia, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.fourteenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.fifteenthOnClick = function () {
        this.activeTitle = 'Rocky Branch Bike Trail';
        this.activeParagraph = 'Rocky Branch Park is a 40-acre mountain bike and hiking park located at the western end of Woodrow Avenue in downtown Belmont. It currently has 2.75 miles of single-track trail that are designed for mountain bikes, walking, and running.';
        this.activeLinkText = 'Visit Rocky Branch Bike Trail >';
        this.activeLinkUrl = 'http://www.mtbproject.com/trail/7007532 ';
        this.activeAddress = '221 W Woodrow Ave, Belmont, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.fifteenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.sixteenthOnClick = function () {
        this.activeTitle = 'Belmont Abbey';
        this.activeParagraph = 'The church at Belmont Abbey, completed in 1893, was once the only abbey cathedral in the nation. In 1998 it was named a Minor Basilica by Rome, a rare Papal honor. Located on the 650-acre campus of Belmont Abbey College, its beautiful painted-glass windows won a gold prize at the Colombian Exposition in 1893.';
        this.activeLinkText = 'Visit Belmont Abbey >';
        this.activeLinkUrl = 'http://www.belmontabbeycollege.edu';
        this.activeAddress = '100 Belmont-Mt. Holly Rd, Belmont, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.sixteenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.seventeenthOnClick = function () {
        this.activeTitle = 'Goat Island Park';
        this.activeParagraph = 'The Goat Island Greenway is .7 miles of paved surface trail, and features an observation pier, two canoe/kayak landings, two picnic shelters, a natural tree house style playground, 18 hole disc golf course, ADA accessible greenway, walking trails, fitness pavilion and an open air amphitheater located in the Town Center.';
        this.activeLinkText = 'Visit Goat Island Park >';
        this.activeLinkUrl = 'http://cramerton.org/goat-island-events/';
        this.activeAddress = '305 Greenwood Pl, Belmont, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.seventeenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.eighteenthOnClick = function () {
        this.activeTitle = 'Seven Oaks Preserve Trail';
        this.activeParagraph = 'This natural surface trail is open to biking and hiking and is located on 78 acres of property conserved by the Catawba Lands Conservancy.  The trail connects to the Daniel Stowe Botanical Garden\'s trails, creating a 5- mile loop from the trailhead.';
        this.activeLinkText = 'Visit Seven Oaks Preserve Trail >';
        this.activeLinkUrl = 'http://www.carolinathreadtrailmap.org/trails/trail/seven-oaks-preserve-trail';
        this.activeAddress = '6900 South New Hope Rd, Belmont, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.eighteenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.nineteenthOnClick = function () {
        this.activeTitle = 'South Fork Trail';
        this.activeParagraph = 'The South Fork Trail is a natural surface trail running along the bank of the South Fork of the Catawba River and connecting the quaint towns of McAdenville and Lowell. The 2-mile trail is suitable for hiking and biking. The trailhead is also adjacent to the R.Y. McAden River Access.';
        this.activeLinkText = 'Visit South Fork Trail >';
        this.activeLinkUrl = 'http://www.carolinathreadtrailmap.org/trails/trail/south-fork-trail-2';
        this.activeAddress = '149 Willow Drive, McAdenville, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.nineteenthhoverrec').addClass('active');
    };
    RecreationComponent.prototype.twentiethOnClick = function () {
        this.activeTitle = 'Avon and Catawba Creeks Greenway';
        this.activeParagraph = 'This paved greenway has several neighborhood entrances in Gastonia. Several small bridges cross over the Catawba Creek. There are numerous areas with information about the various species of birds can be found on this greenway along the trail.';
        this.activeLinkText = 'Visit Avan and Catawba Creeks Greenway >';
        this.activeLinkUrl = 'http://www.carolinathreadtrailmap.org/trails/trail/catawba-creek-greenway ';
        this.activeAddress = '2100 Robinwood Road, Gastonia, NC';
        jQuery('.map-items').removeClass('active');
        jQuery('.twentiethhoverrec').addClass('active');
    };
    RecreationComponent.prototype.ngAfterViewInit = function () {
        jQuery(document).ready(function () {
            new WOW().init();
            /*  Slick Slider Active //COmmunities page
            ============================================= */
            jQuery('.bigslider-wrap').slick({
                dots: false,
                infinite: true,
                nextArrow: 'span.next',
                prevArrow: 'span.prev',
                speed: 800,
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });
    };
    RecreationComponent = __decorate([
        core_1.Component({
            selector: 'recreation-page',
            templateUrl: 'app/recreation.html',
            providers: [platform_browser_1.Title]
        }), 
        __metadata('design:paramtypes', [platform_browser_1.Title])
    ], RecreationComponent);
    return RecreationComponent;
}());
exports.RecreationComponent = RecreationComponent;
//# sourceMappingURL=recreation.component.js.map