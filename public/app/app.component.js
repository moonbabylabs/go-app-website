"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var http_1 = require('@angular/http');
var about_component_1 = require('./about.component');
var communities_component_1 = require('./communities.component');
var recreation_component_1 = require('./recreation.component');
var education_component_1 = require('./education.component');
var business_component_1 = require('./business.component');
var news_component_1 = require('./news.component');
var go_component_1 = require('./go.component');
var privacy_component_1 = require('./privacy.component');
var terms_component_1 = require('./terms.component');
var home_component_1 = require('./home.component');
var email_component_1 = require('./email.component');
var alt_component_1 = require('./alt.component');
var AppComponent = (function () {
    function AppComponent(router) {
        router.subscribe(function (val) {
            ga('set', 'page', '/' + val);
            ga('send', 'pageview');
        });
    }
    AppComponent.prototype.ngAfterViewInit = function () {
        jQuery(document).ready(function () {
            // Fancy box
            jQuery(".various").fancybox({
                maxWidth: 800,
                maxHeight: 600,
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app/layout.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, email_component_1.EmailComponent],
            providers: [http_1.HTTP_PROVIDERS]
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/',
                name: 'Home',
                component: home_component_1.HomeComponent,
                useAsDefault: true
            },
            {
                path: '/about',
                name: 'About',
                component: about_component_1.AboutComponent
            },
            {
                path: '/communities',
                name: 'Communities',
                component: communities_component_1.CommunitiesComponent
            },
            {
                path: '/news',
                name: 'News',
                component: news_component_1.NewsComponent
            },
            {
                path: '/recreation',
                name: 'Recreation',
                component: recreation_component_1.RecreationComponent
            },
            {
                path: '/business',
                name: 'Business',
                component: business_component_1.BusinessComponent
            },
            {
                path: '/go',
                name: 'Go',
                component: go_component_1.GoComponent
            },
            {
                path: '/go/:location',
                name: 'GoGear',
                component: go_component_1.GoComponent
            },
            {
                path: '/go-test',
                name: 'GoTest',
                component: alt_component_1.AltComponent
            },
            {
                path: '/education',
                name: 'Education',
                component: education_component_1.EducationComponent
            },
            {
                path: '/terms',
                name: 'Terms',
                component: terms_component_1.TermsComponent
            },
            {
                path: '/privacy',
                name: 'Privacy',
                component: privacy_component_1.PrivacyComponent
            }
        ]), 
        __metadata('design:paramtypes', [router_deprecated_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map