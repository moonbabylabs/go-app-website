import {bootstrap}    from '@angular/platform-browser-dynamic';
import {AppComponent} from './app.component';
import { Title } from '@angular/platform-browser'; 
import {provide} from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import { ROUTER_PROVIDERS } from '@angular/router-deprecated';

bootstrap(AppComponent, [Title, HTTP_PROVIDERS, ROUTER_PROVIDERS]);
