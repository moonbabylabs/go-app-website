"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_deprecated_1 = require('@angular/router-deprecated');
var Generator = (function () {
    function Generator() {
    }
    return Generator;
}());
exports.Generator = Generator;
var GoComponent = (function () {
    function GoComponent(titleService, router) {
        this.router = router;
        this.file_srcs = [];
        this.generator = {
            text: '',
            imgSrc: '',
            downloadLink: '',
            canvasImage: ''
        };
        this.changePosition = false;
        this.left = 0;
        this.top = 0;
        this.imageTopPosition = 0;
        this.imageLeftPosition = 0;
        titleService.setTitle('GastonOutside - Gear');
    }
    GoComponent.prototype.fileChange = function (input) {
        var _this = this;
        var reader = []; // create empt array for readers
        this.imageTopPosition = 0;
        this.imageLeftPosition = 0;
        reader.push(new FileReader());
        var file = new FileReader();
        reader[0].addEventListener("load", function (event) {
            _this.generator.imgSrc = event.target.result;
            //this.generateImage();
            _this.generator.canvasImage = 'blah blah.jpg';
            // event.target selects the loaded reader
        }, false);
        if (input.files[0]) {
            reader[0].readAsDataURL(input.files[0]);
        }
    };
    GoComponent.prototype.startImagePosition = function () {
        this.top = event.clientY;
        this.left = event.clientX;
    };
    GoComponent.prototype.changeImagePosition = function () {
        this.imageTopPosition = this.imageTopPosition + (event.clientY - this.top);
        this.imageLeftPosition = this.imageLeftPosition + (event.clientX - this.left);
        this.generateImage();
    };
    GoComponent.prototype.calculateAspectRatioFit = function (srcWidth, srcHeight, maxWidth, maxHeight) {
        var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
        return { width: srcWidth * ratio, height: srcHeight * ratio };
    };
    GoComponent.prototype.textInput = function () {
        if (this.generator.imgSrc.length > 2) {
            this.generateImage();
        }
    };
    GoComponent.prototype.preventThrough = function () {
        event.stopPropagation();
        event.preventDefault();
    };
    GoComponent.prototype.dropImage = function () {
        var _this = this;
        event.stopPropagation();
        event.preventDefault();
        //this.generator.imgSrc = event.dataTransfer.files;
        var files = event.dataTransfer.files;
        var reader = [];
        reader.push(new FileReader());
        var file = new FileReader();
        reader[0].addEventListener("load", function (event) {
            _this.generator.imgSrc = event.target.result;
            _this.generator.canvasImage = event.target.result;
            _this.generateImage();
            // event.target selects the loaded reader
        }, false);
        if (files[0]) {
            reader[0].readAsDataURL(files[0]);
        }
        //this.generateImage();
        return false;
    };
    GoComponent.prototype.generateImage = function () {
        var canvas = document.createElement('canvas');
        var img = new Image();
        img.src = this.generator.imgSrc;
        if (img.width > img.height && img.width > 600 && img.height > 600) {
            var ratio = img.width / 600;
            if (img.height / ratio > 600) {
                img.width = 600;
                img.height = img.height / ratio;
            }
            else {
                var heightRatio = img.height / 600;
                img.height = 600;
                img.width = img.width / heightRatio;
            }
        }
        else if (img.height > img.width && img.height > 600 && img.width > 600) {
            var ratio = img.height / 600;
            if (img.width / ratio > 600) {
                img.height = 600;
                img.width = img.width / ratio;
            }
            else {
                var heightRatio = img.width / 600;
                img.width = 600;
                img.height = img.height / heightRatio;
            }
        }
        else if (img.width == img.height && img.height > 600) {
            img.height = 600;
            img.width = 600;
        }
        else {
            img.height = img.height;
            img.width = img.width;
        }
        img.height = img.height + 50;
        img.width = img.width + 50;
        var goLogo = new Image();
        goLogo.src = 'images/footer-logo.png';
        canvas.width = 600;
        canvas.height = 600;
        canvas.getContext('2d').drawImage(img, this.imageLeftPosition, this.imageTopPosition, img.width, img.height);
        canvas.getContext('2d').drawImage(goLogo, 515, 15, 70, 70);
        if (this.generator.text.length > 1) {
            var context = canvas.getContext('2d');
            context.beginPath();
            context.rect(0, 530, 600, 70);
            context.fillStyle = 'rgba(0,119,35, .8)';
            context.fill();
            context.fillStyle = 'white';
            context.font = 'bold 40px Gotham_boldregular';
            context.textAlign = 'center';
            context.fillText('#GO' + this.generator.text, 300, 575);
        }
        this.generator.canvasImage = canvas.toDataURL("image/jpeg");
        jQuery('#drop-uploader').fadeOut();
        jQuery('#custom-man').fadeIn();
    };
    GoComponent.prototype.ngOnDestroy = function () {
        jQuery('body').removeClass('opacity');
    };
    GoComponent.prototype.ngAfterViewInit = function () {
        jQuery('#generatorCanvasImageContainer').draggable();
        console.log('blah blah');
        setTimeout(function () { jQuery('body').addClass('opacity'); }, 800);
        new WOW().init();
        // home slider 
        jQuery('#carousel').slick({
            infinite: true,
            mobileFirst: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: jQuery('.prev'),
            nextArrow: jQuery('.next'),
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        infinite: true
                    }
                },
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true
                    }
                },
                {
                    breakpoint: 735,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 413,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        if (this.router.get('location') === 'gear') {
            jQuery('body').animate({
                scrollTop: jQuery("#gear-container").offset().top
            }, 2000);
        }
        if (this.router.get('location') === 'generator') {
            jQuery('body').animate({
                scrollTop: jQuery("#generator-container").offset().top
            }, 2000);
        }
    };
    GoComponent = __decorate([
        core_1.Component({
            selector: 'go-page',
            templateUrl: 'app/go-new.html',
            providers: [platform_browser_1.Title]
        }), 
        __metadata('design:paramtypes', [platform_browser_1.Title, router_deprecated_1.RouteParams])
    ], GoComponent);
    return GoComponent;
}());
exports.GoComponent = GoComponent;
//# sourceMappingURL=go.component.js.map