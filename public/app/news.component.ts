import {Component, AfterViewInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

declare var jQuery: any;

declare var WOW: any;

@Component({
    selector: 'news-page',
    templateUrl: 'app/news-updated.html',
    providers: [Title]
})


export class NewsComponent implements AfterViewInit { 

	constructor( titleService: Title) {

		titleService.setTitle('GastonOutside - News' );

	}

	ngAfterViewInit() {

		jQuery(document).ready( function () {

			jQuery('#fixedMenu').affix({
				offset: {
					top: jQuery('#fixedMenu').offset().top,
					bottom: (jQuery('footer').outerHeight(true) + jQuery('.news-letter').outerHeight(true)) + 40
				}
			});

			new WOW().init();
			!function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } (document, "script", "twitter-wjs");

		});

	}

}
