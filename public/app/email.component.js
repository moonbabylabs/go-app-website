"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var EmailComponent = (function () {
    function EmailComponent(http) {
        this.message = 'Enter email for Gaston Outside Updates';
        this.inputMessage = '';
        this.thankYouHeadline = '';
        this.thankYouCopy = '';
        this.http = http;
    }
    EmailComponent.prototype.submit = function () {
        var _this = this;
        var test = '';
        if (this.inputMessage.length > 0 && this.inputMessage.indexOf('@') > -1) {
            this.message = 'Thank you for registering!';
            this.thankYouCopy = 'WE WILL BE IN TOUCH SOON WITH AWESOME UPDATES!';
            jQuery('.email-form').addClass('hide');
            jQuery('.email-message').css({ 'width': '100%', 'textAlign': 'center' });
            setTimeout(function () {
                jQuery('.news-letter.homepage-newsletter').fadeOut();
            }, 3300);
            this.http.get('/email?address=' + this.inputMessage)
                .map(function (res) { return res.text(); })
                .subscribe(function (data) { return _this.email = data; }, function (err) { return console.log('error'); });
        }
    };
    EmailComponent = __decorate([
        core_1.Component({
            selector: 'email-signup	',
            templateUrl: 'app/email.html'
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], EmailComponent);
    return EmailComponent;
}());
exports.EmailComponent = EmailComponent;
//# sourceMappingURL=email.component.js.map