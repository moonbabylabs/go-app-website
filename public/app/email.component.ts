import {Component} from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';


declare var jQuery: any;

@Component({
    selector: 'email-signup	',
    templateUrl: 'app/email.html'
})

export class EmailComponent {

	message = 'Enter email for Gaston Outside Updates';

	inputMessage = '';

	thankYouHeadline = '';

	thankYouCopy = '';

	email: string;

	http: any;

	constructor( http: Http ) {

		this.http = http;

	}


	submit() {
		
		var test = '';

		if (this.inputMessage.length > 0 && this.inputMessage.indexOf('@') > -1) {

			this.message = 'Thank you for registering!';

			this.thankYouCopy = 'WE WILL BE IN TOUCH SOON WITH AWESOME UPDATES!';

			jQuery('.email-form').addClass('hide');

			jQuery('.email-message').css({ 'width': '100%', 'textAlign' : 'center' });


			setTimeout(function () {

				jQuery('.news-letter.homepage-newsletter').fadeOut();
			}, 3300);

			this.http.get('/email?address=' + this.inputMessage)
				.map(res => res.text())
				.subscribe(
				data => this.email = data,
				err => console.log('error')
				);

		}


	}


}