import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';

declare var jQuery: any;

declare var WOW: any;

declare var businessContent: any;



@Component({
    selector: 'business-page',
    templateUrl: 'app/business-new.html',
    providers: [ Title ]
})


export class BusinessComponent implements AfterViewInit, OnDestroy {

	content: any;


	constructor( private titleService: Title) {

		titleService.setTitle('GastonOutside - Business');

		this.content = businessContent;

		console.log(this.content);

	}


	ngOnDestroy() {

	}

	ngAfterViewInit() {


		jQuery(document).ready( function () {

			new WOW().init();


			/*  Slick Slider Active //Business page
			============================================= */
			jQuery('.education-slider').slick({
				dots: false,
				infinite: true,
				nextArrow: 'span.next',
				prevArrow: 'span.prev',
				speed: 800,
				autoplay: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});

		});

	}
 }
