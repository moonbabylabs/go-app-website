import {Component, AfterViewInit, Inject, OnInit, OnDestroy} from '@angular/core';
import { Title } from '@angular/platform-browser';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router-deprecated';

declare var jQuery: any;

declare var WOW: any;

declare var YT: any;

declare var ga: any;



@Component({
    selector: 'home-page',
    templateUrl: 'app/home-new.html',
    providers : [ Title ]
})

@Injectable()
export class HomeComponent implements AfterViewInit, OnDestroy {



	constructor(titleService: Title, private router: Router) {

		titleService.setTitle('GastonOutside');

		ga('send', 'pageview');

	}

	playVideo() {

		jQuery('.jumbotron.main-image').html('<iframe src="https://player.vimeo.com/video/168881256?title=0&byline=0&portrait=0&autoplay=true" style="width: 100%;" height="460" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');

	}

	onClickGeneratorLink() {
		this.router.navigateByUrl('/go/generator');

	}

	onClickGearLink() {

		this.router.navigateByUrl('/go/gear');

	}

	ngOnDestroy() {

		setTimeout(function() {

			jQuery('#homeContainer').addClass('fadeOutLeft');
		}, 1000);
			
	}

    ngAfterViewInit() {


		jQuery(document).ready(function() {


			new WOW().init();
			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https'; js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}(document,"script","twitter-wjs");
			// home slider 
			jQuery('#carousel').slick({
				infinite: true,
				mobileFirst: true,
				autoplay: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: jQuery('.prev'),
				nextArrow: jQuery('.next'),
				responsive: [
					{
						breakpoint: 1600,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 1,
							infinite: true
						}
					},
					{
						breakpoint: 1000,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
							infinite: true
						}
					},
					{
						breakpoint: 735,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 413,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 1,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 1
						}
					}
				]
			});
		});

	}

 }
