import {Component, AfterViewInit} from '@angular/core';
import { Title } from '@angular/platform-browser';

declare var jQuery;

declare var WOW: any;

@Component({
    selector: 'communities-page',
    templateUrl: 'app/communities-updated.html',
	providers: [ Title ]
})


export class CommunitiesComponent implements AfterViewInit {

	activeTitle: string;
	activeParagraph: string;
	activeLinkUrl: string;
	activeLinkText: string;

	constructor( private titleService: Title) {

		titleService.setTitle('GastonOutside - Communities Page');

		this.activeTitle = 'Gastonia';
		this.activeParagraph = 'Located centrally, Gastonia is the county’s largest city and the county seat. The city’s business community and residential neighborhoods continue to grow as more and more people make Gastonia their home.';
		this.activeLinkText = 'Visit Gastonia >';
		this.activeLinkUrl = 'http://www.cityofgastonia.com/';

	}

	firstOnClick() {


		this.activeTitle = 'Belmont';
		this.activeParagraph = 'Charming, growing and located just outside of Charlotte, Belmont is home to Belmont Abbey College, plenty of young families and a vibrant downtown.';
		this.activeLinkText = 'Visit Belmont >';
		this.activeLinkUrl = 'http://www.cityofbelmont.org/';


		jQuery('.map-items').removeClass('active');
		jQuery('.thirdhover').addClass('active');
	}

	secondOnClick() {

		this.activeTitle = 'Bessemer City';
		this.activeParagraph = 'Industry gave Bessemer City its name in 19th century, and industry still drives the city today with a thriving trade in mining, manufacturing and farming.';
		this.activeLinkText = 'Visit Bessemer City >';
		this.activeLinkUrl = 'http://bessemercity.com/ ';

		jQuery('.map-items').removeClass('active');
		jQuery('.secondhover').addClass('active');

	}


	thirdOnClick() {

		this.activeTitle = 'Cherryville';
		this.activeParagraph = 'Named after the town’s beautiful cherry trees, Cherryville is a charming community rich in county history, boasting several museums and historic attractions.';
		this.activeLinkText = 'Visit Cherryville >';
		this.activeLinkUrl = 'http://www.cityofcherryville.com/ ';

		jQuery('.map-items').removeClass('active');
		jQuery('.eighthover').addClass('active');

	}

	fourthOnClick() {

		this.activeTitle = 'Cramerton';
		this.activeParagraph = 'Crossed by the beautiful South Fork of the Catawba River, Cramerton is a peaceful residential community conveniently located just south of Belmont and McAdenville.';
		this.activeLinkText = 'Visit Cramerton >';
		this.activeLinkUrl = 'http://cramerton.org/';

		jQuery('.map-items').removeClass('active');
		jQuery('.sevenhover').addClass('active');

	}

	fifthOnClick() {

		this.activeTitle = 'Dallas';
		this.activeParagraph = 'The oldest town in the county, Dallas boasts a pleasant mix of historic homes, modern neighborhoods and vibrant retail, making this community a great place to live. ';
		this.activeLinkText = 'Visit Dallas >';
		this.activeLinkUrl = 'http://www.dallasnc.net/ ';

		jQuery('.map-items').removeClass('active');
		jQuery('.fivehover').addClass('active');

	}

	sixthOnClick() {

		this.activeTitle = 'Gastonia';
		this.activeParagraph = 'Located centrally, Gastonia is the county’s largest city and the county seat. The city’s business community and residential neighborhoods continue to grow as more and more people make Gastonia their home.';
		this.activeLinkText = 'Visit Gastonia >';
		this.activeLinkUrl = 'http://www.cityofgastonia.com/';

		jQuery('.map-items').removeClass('active');
		jQuery('.sixhover').addClass('active');

	}

	seventhOnClick() {

		this.activeTitle = 'Bessemer City';
		this.activeParagraph = 'Industry gave Bessemer City its name in 19th century, and industry still drives the city today with a thriving trade in mining, manufacturing and farming.';
		this.activeLinkText = 'Visit Bessemer City >';
		this.activeLinkUrl = 'http://bessemercity.com/ ';

		jQuery('.map-items').removeClass('active');
		jQuery('.secondhover').addClass('active');


	}

	eigthOnClick() {

		this.activeTitle = 'High Shoals';
		this.activeParagraph = 'Known for its rich heritage in textile manufacturing, High Shoals is the county’s newest municipality and features a welcoming, small town atmosphere.';
		this.activeLinkText = 'Visit High Shoals >';
		this.activeLinkUrl = 'http://highshoalsnc.ning.com/ ';

		jQuery('.map-items').removeClass('active');
		jQuery('.thirteenhover').addClass('active');

	}

	ninthOnClick() {

		this.activeTitle = 'Kings Mountain';
		this.activeParagraph = 'Nestled at the foothills of the Blue Ridge Mountains, Kings Mountain offers an ideal balance of business, community life and outdoor pursuits.';
		this.activeLinkText = 'Visit Kings Mountain >';
		this.activeLinkUrl = 'http://www.cityofkm.com/';

		jQuery('.map-items').removeClass('active');
		jQuery('.fourteenhover').addClass('active');

	}

	tenthOnClick() {

		this.activeTitle = 'Lowell';
		this.activeParagraph = 'Proximity to some of the area’s top attractions—like the US National Whitewater Center and the South Fork River—makes Lowell a sweet spot to settle or explore.';
		this.activeLinkText = 'Visit Lowell >';
		this.activeLinkUrl = 'http://www.lowellnc.com/';

		jQuery('.map-items').removeClass('active');
		jQuery('.ninehover').addClass('active');

	}


	twelthOnClick() {

		this.activeTitle = 'McAdenville';
		this.activeParagraph = 'McAdenville draws thousands of visitors each year with its glittering holiday display—but the small town’s growing charms are turning it into a year-round destination.';
		this.activeLinkText = 'Visit McAdenville >';
		this.activeLinkUrl = 'http://www.townofmcadenville.org/';

		jQuery('.map-items').removeClass('active');
		jQuery('.fourhover').addClass('active');

	}

	thirteenthOnClick() {

		this.activeTitle = 'Mt. Holly';
		this.activeParagraph = 'Nestled on the bank of the Catawba River, Mount Holly is growing rapidly, with its well-developed retail and residential neighborhoods attracting families of all ages.';
		this.activeLinkText = 'Visit Mt. Holly >';
		this.activeLinkUrl = 'http://www.mtholly.us/ ';

		jQuery('.map-items').removeClass('active');
		jQuery('.onehover').addClass('active');

	}

	fourteenthOnClick() {

		this.activeTitle = 'Ranlo';
		this.activeParagraph = 'Textile manufacturing has always played an important role in Ranlo’s growth, and the small town is now dotted with historic mill homes and bungalows.';
		this.activeLinkText = 'Visit Ranlo >';
		this.activeLinkUrl = 'http://ranlo.org/ ';

		jQuery('.map-items').removeClass('active');
		jQuery('.elevenhover').addClass('active');

	}

	fifteenthOnClick() {

		this.activeTitle = 'Spencer Mountain';
		this.activeParagraph = 'Small, quiet and nestled at the base of a mountainous ridge, this community offers some of the county’s most beautiful views and river access.';
		this.activeLinkText = '';
		this.activeLinkUrl = '';

		jQuery('.map-items').removeClass('active');
		jQuery('.fifteenthhover').addClass('active');

	}

	sixteenthOnClick() {

		jQuery.each(function (e) {


		});
		this.activeTitle = 'Stanley';
		this.activeParagraph = 'Named after one of its original settlers, Stanley is one of the oldest towns in the county. Today, this friendly community is home to a number of major industries.';
		this.activeLinkText = 'Visit Stanley >';
		this.activeLinkUrl = 'https://townofstanley.org/';

		jQuery('.map-items').removeClass('active');
		jQuery('.sixteenthhover').addClass('active');

	}


	ngAfterViewInit( ) {

		jQuery(document).ready( function () {

			new WOW().init();

			/*  Slick Slider Active //COmmunities page
			============================================= */
			jQuery('.bigslider-wrap').slick({
				dots: false,
				infinite: true,
				nextArrow: 'span.next',
				prevArrow: 'span.prev',
				speed: 800,
				autoplay: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});

		});

	}

 }
