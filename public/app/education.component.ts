import {Component, AfterViewInit} from '@angular/core';
import { Title } from '@angular/platform-browser';

declare var jQuery;

declare var WOW;
 
@Component({
    selector: 'education-page',
    templateUrl: 'app/education.html',
	providers: [ Title ]
})


export class EducationComponent implements AfterViewInit {
	

	constructor( private titleService: Title) {

		titleService.setTitle('GastonOutside - Education');
	}

	ngAfterViewInit() {

		jQuery(document).ready( function () {

			new WOW().init();

			/*  Slick Slider Active //Business page
			============================================= */
			jQuery('.education-slider').slick({
				dots: false,
				infinite: true,
				nextArrow: 'span.next',
				prevArrow: 'span.prev',
				speed: 800,
				autoplay: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});

		});

	}

}
