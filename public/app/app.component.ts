import {Component, ElementRef, OnInit, AfterViewInit} from '@angular/core';
import { Router, RouteConfig, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { HTTP_PROVIDERS } from '@angular/http';

import { AboutComponent } from './about.component';
import { CommunitiesComponent } from './communities.component';
import { RecreationComponent } from './recreation.component';
import { EducationComponent } from './education.component';
import { BusinessComponent } from './business.component';
import { NewsComponent } from './news.component';
import { GoComponent } from './go.component';
import { PrivacyComponent } from './privacy.component';
import { TermsComponent } from './terms.component';
import { HomeComponent } from './home.component';
import { EmailComponent } from './email.component';
import { AltComponent } from './alt.component';
declare var WOW: any;

declare var jQuery: any;

declare var slick: any;

declare var ga: any;

@Component({
    selector: 'my-app',
    templateUrl: 'app/layout.html',
    directives: [ROUTER_DIRECTIVES, EmailComponent],
	providers: [HTTP_PROVIDERS]
})

@RouteConfig([
  {
    path: '/',
    name: 'Home',
    component: HomeComponent,
    useAsDefault: true
  },

  {
    path: '/about',
    name: 'About',
    component: AboutComponent
  },
  {
    path: '/communities',
    name: 'Communities',
    component: CommunitiesComponent
  },
  {
    path: '/news',
    name: 'News',
    component: NewsComponent
  },
  {
    path: '/recreation',
    name: 'Recreation',
    component: RecreationComponent
  },
  {
    path: '/business',
    name: 'Business',
    component: BusinessComponent
  },
  {
    path: '/go',
    name: 'Go',
    component: GoComponent
  },
  {
    path: '/go/:location',
    name: 'GoGear',
    component: GoComponent
  },
  { 
    path: '/go-test',
    name: 'GoTest',
    component: AltComponent
  },
  {
    path: '/education',
    name: 'Education',
    component: EducationComponent
  },
  {
    path: '/terms',
    name: 'Terms',
    component: TermsComponent
  },
  {
    path: '/privacy',
    name: 'Privacy',
    component: PrivacyComponent
  }
])

export class AppComponent implements AfterViewInit { 


    constructor( router ?: Router ) {

      router.subscribe((val) => {
        ga('set', 'page', '/' + val);
        ga('send', 'pageview');
      });  

    }

    ngAfterViewInit() {

      jQuery(document).ready(function() {


    // Fancy box
    jQuery(".various").fancybox({
      maxWidth: 800,
      maxHeight: 600,
      fitToView: false,
      width: '70%',
      height: '70%',
      autoSize: false,
      closeClick: false,
      openEffect: 'none',
      closeEffect: 'none'
    });
 });

    }

}
