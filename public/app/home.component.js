"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var core_2 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var HomeComponent = (function () {
    function HomeComponent(titleService, router) {
        this.router = router;
        titleService.setTitle('GastonOutside');
        ga('send', 'pageview');
    }
    HomeComponent.prototype.playVideo = function () {
        jQuery('.jumbotron.main-image').html('<iframe src="https://player.vimeo.com/video/168881256?title=0&byline=0&portrait=0&autoplay=true" style="width: 100%;" height="460" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
    };
    HomeComponent.prototype.onClickGeneratorLink = function () {
        this.router.navigateByUrl('/go/generator');
    };
    HomeComponent.prototype.onClickGearLink = function () {
        this.router.navigateByUrl('/go/gear');
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        setTimeout(function () {
            jQuery('#homeContainer').addClass('fadeOutLeft');
        }, 1000);
    };
    HomeComponent.prototype.ngAfterViewInit = function () {
        jQuery(document).ready(function () {
            new WOW().init();
            !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); }(document, "script", "twitter-wjs");
            // home slider 
            jQuery('#carousel').slick({
                infinite: true,
                mobileFirst: true,
                autoplay: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                prevArrow: jQuery('.prev'),
                nextArrow: jQuery('.next'),
                responsive: [
                    {
                        breakpoint: 1600,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 735,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 413,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home-page',
            templateUrl: 'app/home-new.html',
            providers: [platform_browser_1.Title]
        }),
        core_2.Injectable(), 
        __metadata('design:paramtypes', [platform_browser_1.Title, router_deprecated_1.Router])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map