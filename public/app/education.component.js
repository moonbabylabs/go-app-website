"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var EducationComponent = (function () {
    function EducationComponent(titleService) {
        this.titleService = titleService;
        titleService.setTitle('GastonOutside - Education');
    }
    EducationComponent.prototype.ngAfterViewInit = function () {
        jQuery(document).ready(function () {
            new WOW().init();
            /*  Slick Slider Active //Business page
            ============================================= */
            jQuery('.education-slider').slick({
                dots: false,
                infinite: true,
                nextArrow: 'span.next',
                prevArrow: 'span.prev',
                speed: 800,
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });
    };
    EducationComponent = __decorate([
        core_1.Component({
            selector: 'education-page',
            templateUrl: 'app/education.html',
            providers: [platform_browser_1.Title]
        }), 
        __metadata('design:paramtypes', [platform_browser_1.Title])
    ], EducationComponent);
    return EducationComponent;
}());
exports.EducationComponent = EducationComponent;
//# sourceMappingURL=education.component.js.map