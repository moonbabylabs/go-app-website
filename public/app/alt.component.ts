import {Component, HostListener, AfterViewInit, OnDestroy} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { RouteParams } from '@angular/router-deprecated';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

declare var jQuery: any;

declare var WOW: any;

declare var Slick: any;

declare var event: any;


export class Generator {

	text : string

	imgSrc : any

	downloadLink : any

	canvasImage : any

	hashtagContainer : any

	image : any

	loadingImgDisplay: string

	canvasContainerBackgroundColor: string

	introTextDisplay: string

	logo: any

};


@Component({
    selector: 'go-page',
    templateUrl: 'app/go-new-test.html',
    providers: [Title]
})
	
export class AltComponent implements AfterViewInit, OnDestroy { 

	private location;

	private http;

	file_srcs: string[] = [];

	generator: Generator = {

		text: '',

		imgSrc: '',

		downloadLink: '',

		canvasImage: '',

		loadingImgDisplay: '',

		introTextDisplay: '',

		canvasContainerBackgroundColor: '',

		hashtagContainer : {

			display: 'none'

		},

		image : {

			height: "0",

			width: "0",

			left: 0,

			top: 0

		},

		logo: {

			display: 'none'

		}

	};

	changePosition = false;

	left = 0;

	top = 0;

	imageTopPosition = 0;

	imageLeftPosition = 0;


	constructor( titleService: Title, private router: RouteParams, http: Http) {

		titleService.setTitle('GastonOutside - Gear');

		this.http = http;

		this.generator.hashtagContainer.display = 'none';

		this.generator.image.height = 0;

		this.generator.image.width = 0;

		this.generator.image.left = 0;

		this.generator.image.top = 0;

		this.generator.loadingImgDisplay = 'none';

		this.generator.canvasContainerBackgroundColor = '#ddd';

		this.generator.introTextDisplay = 'block';

		this.generator.logo.display = 'none';

	}


	fileChange(input) {
        var reader = [];  // create empt array for readers
		
        var img = new Image();

        reader.push(new FileReader());

        var file = new FileReader();

        this.generator.canvasContainerBackgroundColor = '#000';

        this.generator.introTextDisplay = 'none';

        this.generator.loadingImgDisplay = 'block';

        this.generator.imgSrc = '';

        this.generator.image.left = 0;

        this.generator.image.top = 0;

        this.generator.image.display = 'none';


        if (input.files[0]) {
        
            reader[0].readAsDataURL(input.files[0]);
		        
        }

        
    	img.addEventListener("load", (event) => {
			console.log('this is test');	
    		var imgSizes = this.calculateAspectRatioFit(img.width, img.height, 600, 600);    		
    	//	this.generator.image.width = imgSizes.width + 10;
    	//	this.generator.image.height = imgSizes.height + 10;

		   // var imgSizes = this.calculateAspectRatioFit(img.width, img.height, 400, 400);    		
    		this.generator.image.width = imgSizes.width ;
    		this.generator.image.height = imgSizes.height ;


    		this.generator.loadingImgDisplay = 'none';

    		this.generator.image.display = 'inline';

    		this.generator.logo.display = 'block';

    	 });

        reader[0].addEventListener("load", (event) => {
            
            this.generator.imgSrc = event.target.result;

            img.src = event.target.result;

			this.generator.canvasImage = event.target.result;

			this.generateImage();

        }, false);


        

	}


	calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {


		if (srcWidth > srcHeight && srcWidth > 600 && srcHeight > 600) {

			var ratio = srcWidth / 600;


			if (srcHeight / ratio > 600) {

				srcWidth = 600;

				srcHeight = srcHeight / ratio;

			} else {

				var heightRatio = srcHeight / 600;

				srcHeight = 600;

				srcWidth = srcWidth / heightRatio;
			}


		} else if (srcHeight > srcWidth && srcHeight > 600 && srcWidth > 600) {

			var ratio = srcHeight / 600;


			if (srcWidth / ratio > 600) {

				srcHeight = 600;

				srcWidth = srcWidth / ratio;

			} else {

				var heightRatio = srcWidth / 600;

				srcWidth = 600;

				srcHeight = srcHeight / heightRatio;
			}


		} else if (srcWidth == srcHeight && srcHeight > 600) {

			srcHeight = 600;

			srcWidth = 600;

		} else {

			srcHeight = srcHeight;

			srcWidth = srcWidth;
		}	

		return {

			width : srcWidth,

			height : srcHeight
		}

	}

	textInput() {

		if (this.generator.imgSrc.length > 2) {

			this.generateImage();
		}

	}

	preventThrough() {

		event.stopPropagation();
		event.preventDefault();

	}


	generateHashtag() {

		if ( this.generator.text.length > 0 ) {

			this.generator.hashtagContainer.display = 'block';

			this.generateImage();
		
		} else {

			this.generator.hashtagContainer.display = 'none';
		}


	}


	generatorImgMouseup( e ) {

		this.generator.image.left = parseFloat(e.style.left);

		this.generator.image.top = parseFloat(e.style.top);

		this.generateImage();

	}


	downloadImage() {
	    
	    var body = JSON.stringify({image : this.generator.canvasImage});
	    
    	var headers = new Headers({ 'Content-Type': 'application/json' });

		this.http.post('/download', body, headers)
						.map(res => {
                            var url = '/image?img=' + res.text();
							window.open('/image?img=' + res.text());
							//window.location.href = url
						})
						.subscribe(
						data => {

							console.log(data);

						},
						err => console.log('error')
						);

	}


	generateImage() {

		var canvas = document.createElement('canvas');

		var img = new Image();

		img.src = this.generator.imgSrc;

		var goLogo = new Image();

		goLogo.src = 'images/footer-logo.png';

		canvas.width = 400;//600;

		canvas.height = 400; //600;

		canvas.getContext('2d').drawImage(img, this.generator.image.left, this.generator.image.top, this.generator.image.width, this.generator.image.height);

		canvas.getContext('2d').drawImage(goLogo, 320, 15, 70, 70);


		if ( this.generator.text.length > 1 ) {

			var context = canvas.getContext('2d');

			context.beginPath();
			context.rect(0, 330, 400, 70);
			context.fillStyle = 'rgba(0,119,35, .8)';
			context.fill();

			context.fillStyle = 'white';

			//context.font = 'bold 40px Gotham_boldregular';
			context.font = 'bold 30px Gotham_boldregular'; 

			context.textAlign = 'center';

			context.fillText('#GO' + this.generator.text, 200, 380);

		}


		this.generator.canvasImage = canvas.toDataURL("image/jpeg");
	}



	ngOnDestroy() {

		jQuery('body').removeClass('opacity');

	}

	ngAfterViewInit() {

		jQuery('#custom-man').draggable();
       

		setTimeout(function() { jQuery('body').addClass('opacity') }, 800);



			new WOW().init();


			// home slider 
			jQuery('#carousel').slick({
				infinite: true,
				mobileFirst: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: jQuery('.prev'),
				nextArrow: jQuery('.next'),
				responsive: [
					{
						breakpoint: 1600,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 1,
							infinite: true
						}
					},
					{
						breakpoint: 1000,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
							infinite: true
						}
					},
					{
						breakpoint: 735,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 413,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 1,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					}
				]
			});


		if (this.router.get('location') === 'gear') {

			jQuery('body').animate({
				scrollTop: jQuery("#gear-container").offset().top
			}, 2000);

		}

		if (this.router.get('location') === 'generator') {

			jQuery('body').animate({

				scrollTop: jQuery("#generator-container").offset().top

			}, 2000);
		}

	}

}
