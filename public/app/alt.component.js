"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var router_deprecated_1 = require('@angular/router-deprecated');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var Generator = (function () {
    function Generator() {
    }
    return Generator;
}());
exports.Generator = Generator;
;
var AltComponent = (function () {
    function AltComponent(titleService, router, http) {
        this.router = router;
        this.file_srcs = [];
        this.generator = {
            text: '',
            imgSrc: '',
            downloadLink: '',
            canvasImage: '',
            loadingImgDisplay: '',
            introTextDisplay: '',
            canvasContainerBackgroundColor: '',
            hashtagContainer: {
                display: 'none'
            },
            image: {
                height: "0",
                width: "0",
                left: 0,
                top: 0
            },
            logo: {
                display: 'none'
            }
        };
        this.changePosition = false;
        this.left = 0;
        this.top = 0;
        this.imageTopPosition = 0;
        this.imageLeftPosition = 0;
        titleService.setTitle('GastonOutside - Gear');
        this.http = http;
        this.generator.hashtagContainer.display = 'none';
        this.generator.image.height = 0;
        this.generator.image.width = 0;
        this.generator.image.left = 0;
        this.generator.image.top = 0;
        this.generator.loadingImgDisplay = 'none';
        this.generator.canvasContainerBackgroundColor = '#ddd';
        this.generator.introTextDisplay = 'block';
        this.generator.logo.display = 'none';
    }
    AltComponent.prototype.fileChange = function (input) {
        var _this = this;
        var reader = []; // create empt array for readers
        var img = new Image();
        reader.push(new FileReader());
        var file = new FileReader();
        this.generator.canvasContainerBackgroundColor = '#000';
        this.generator.introTextDisplay = 'none';
        this.generator.loadingImgDisplay = 'block';
        this.generator.imgSrc = '';
        this.generator.image.left = 0;
        this.generator.image.top = 0;
        this.generator.image.display = 'none';
        if (input.files[0]) {
            reader[0].readAsDataURL(input.files[0]);
        }
        img.addEventListener("load", function (event) {
            //var imgSizes = this.calculateAspectRatioFit(img.width, img.height, 600, 600);    		
            //	this.generator.image.width = imgSizes.width + 10;
            //	this.generator.image.height = imgSizes.height + 10;
            var imgSizes = _this.calculateAspectRatioFit(img.width, img.height, 400, 400);
            _this.generator.image.width = imgSizes.width;
            _this.generator.image.height = imgSizes.height;
            _this.generator.loadingImgDisplay = 'none';
            _this.generator.image.display = 'inline';
            _this.generator.logo.display = 'block';
        });
        reader[0].addEventListener("load", function (event) {
            _this.generator.imgSrc = event.target.result;
            img.src = event.target.result;
            _this.generator.canvasImage = event.target.result;
            _this.generateImage();
        }, false);
    };
    AltComponent.prototype.calculateAspectRatioFit = function (srcWidth, srcHeight, maxWidth, maxHeight) {
        if (srcWidth > srcHeight && srcWidth > 600 && srcHeight > 600) {
            var ratio = srcWidth / 600;
            if (srcHeight / ratio > 600) {
                srcWidth = 600;
                srcHeight = srcHeight / ratio;
            }
            else {
                var heightRatio = srcHeight / 600;
                srcHeight = 600;
                srcWidth = srcWidth / heightRatio;
            }
        }
        else if (srcHeight > srcWidth && srcHeight > 600 && srcWidth > 600) {
            var ratio = srcHeight / 600;
            if (srcWidth / ratio > 600) {
                srcHeight = 600;
                srcWidth = srcWidth / ratio;
            }
            else {
                var heightRatio = srcWidth / 600;
                srcWidth = 600;
                srcHeight = srcHeight / heightRatio;
            }
        }
        else if (srcWidth == srcHeight && srcHeight > 600) {
            srcHeight = 600;
            srcWidth = 600;
        }
        else {
            srcHeight = srcHeight;
            srcWidth = srcWidth;
        }
        return {
            width: srcWidth,
            height: srcHeight
        };
    };
    AltComponent.prototype.textInput = function () {
        if (this.generator.imgSrc.length > 2) {
            this.generateImage();
        }
    };
    AltComponent.prototype.preventThrough = function () {
        event.stopPropagation();
        event.preventDefault();
    };
    AltComponent.prototype.generateHashtag = function () {
        if (this.generator.text.length > 0) {
            this.generator.hashtagContainer.display = 'block';
            this.generateImage();
        }
        else {
            this.generator.hashtagContainer.display = 'none';
        }
    };
    AltComponent.prototype.generatorImgMouseup = function (e) {
        this.generator.image.left = parseFloat(e.style.left);
        this.generator.image.top = parseFloat(e.style.top);
        this.generateImage();
    };
    AltComponent.prototype.downloadImage = function () {
        var body = JSON.stringify({ image: this.generator.canvasImage });
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.http.post('/download', body, headers)
            .map(function (res) {
            var url = '/image?img=' + res.text();
            //window.open('/image?img=' + res.text());
            window.location.href = url;
        })
            .subscribe(function (data) {
            console.log(data);
        }, function (err) { return console.log('error'); });
    };
    AltComponent.prototype.generateImage = function () {
        var canvas = document.createElement('canvas');
        var img = new Image();
        img.src = this.generator.imgSrc;
        var goLogo = new Image();
        goLogo.src = 'images/footer-logo.png';
        canvas.width = 400; //600;
        canvas.height = 400; //600;
        canvas.getContext('2d').drawImage(img, this.generator.image.left, this.generator.image.top, this.generator.image.width, this.generator.image.height);
        canvas.getContext('2d').drawImage(goLogo, 320, 15, 70, 70); 
        if (this.generator.text.length > 1) {
            var context = canvas.getContext('2d');
            context.beginPath();
            context.rect(0, 330, 400, 70);
            context.fillStyle = 'rgba(0,119,35, .8)';
            context.fill();
            context.fillStyle = 'white';
            //context.font = 'bold 40px Gotham_boldregular';
            context.font = 'bold 30px Gotham_boldregular';
            context.textAlign = 'center';
            context.fillText('#GO' + this.generator.text, 200, 380);
        }
        this.generator.canvasImage = canvas.toDataURL("image/jpeg");
    };
    AltComponent.prototype.ngOnDestroy = function () {
        jQuery('body').removeClass('opacity');
    };
    AltComponent.prototype.ngAfterViewInit = function () {
        jQuery('#custom-man').draggable();
        setTimeout(function () { jQuery('body').addClass('opacity'); }, 800);
        new WOW().init();
        // home slider 
        jQuery('#carousel').slick({
            infinite: true,
            mobileFirst: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: jQuery('.prev'),
            nextArrow: jQuery('.next'),
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        infinite: true
                    }
                },
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true
                    }
                },
                {
                    breakpoint: 735,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 413,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        if (this.router.get('location') === 'gear') {
            jQuery('body').animate({
                scrollTop: jQuery("#gear-container").offset().top
            }, 2000);
        }
        if (this.router.get('location') === 'generator') {
            jQuery('body').animate({
                scrollTop: jQuery("#generator-container").offset().top
            }, 2000);
        }
    };
    AltComponent = __decorate([
        core_1.Component({
            selector: 'go-page',
            templateUrl: 'app/go-new-test.html',
            providers: [platform_browser_1.Title]
        }), 
        __metadata('design:paramtypes', [platform_browser_1.Title, router_deprecated_1.RouteParams, http_1.Http])
    ], AltComponent);
    return AltComponent;
}());
exports.AltComponent = AltComponent;
//# sourceMappingURL=alt.component.js.map