import {Component, HostListener, AfterViewInit, OnDestroy} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { RouteParams } from '@angular/router-deprecated';

declare var jQuery: any;

declare var WOW: any;

declare var Slick: any;

declare var event: any;

export class Generator {

	text : string

	imgSrc : any

	downloadLink : any

	canvasImage : any


}


@Component({
    selector: 'go-page',
    templateUrl: 'app/go-new.html',
    providers: [Title]
})
	
export class GoComponent implements AfterViewInit, OnDestroy { 

	private location;

	private http;

	file_srcs: string[] = [];

	generator: Generator = {

		text: '',

		imgSrc: '',

		downloadLink: '',

		canvasImage: ''
	};

	changePosition = false;

	left = 0;

	top = 0;

	imageTopPosition = 0;

	imageLeftPosition = 0;

	constructor( titleService: Title, private router: RouteParams) {


		titleService.setTitle('GastonOutside - Gear');

	}


	fileChange(input) {
        var reader = [];  // create empt array for readers

        this.imageTopPosition = 0;
        this.imageLeftPosition = 0;

        reader.push(new FileReader());

        var file = new FileReader();

            reader[0].addEventListener("load", (event) => {
                
                this.generator.imgSrc = event.target.result;

				//this.generateImage();

				this.generator.canvasImage = 'blah blah.jpg';

                // event.target selects the loaded reader
            }, false);


            if (input.files[0]) {
            
                reader[0].readAsDataURL(input.files[0]);
            
            }


	}

	startImagePosition() {

		this.top = event.clientY;
		this.left = event.clientX;

	}

	changeImagePosition() {

		this.imageTopPosition = this.imageTopPosition + (event.clientY - this.top);
		this.imageLeftPosition = this.imageLeftPosition + (event.clientX - this.left); 

		this.generateImage();
		
	}



	calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

	    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

	    	return { width: srcWidth * ratio, height: srcHeight * ratio };

	}

	textInput() {

		if (this.generator.imgSrc.length > 2) {

			this.generateImage();
		}

	}

	preventThrough() {

		event.stopPropagation();
		event.preventDefault();

	}

	dropImage() {
		event.stopPropagation();
		event.preventDefault();
		//this.generator.imgSrc = event.dataTransfer.files;

		var files = event.dataTransfer.files;

        var reader = []; 


        reader.push(new FileReader());

            var file = new FileReader();


            reader[0].addEventListener("load", (event) => {
                this.generator.imgSrc = event.target.result;

                this.generator.canvasImage = event.target.result;
                this.generateImage();

                // event.target selects the loaded reader
            }, false);

            if (files[0]) {
                reader[0].readAsDataURL(files[0]);
            }


		//this.generateImage();
		return false;
	}

	generateImage() {

		var canvas = document.createElement('canvas');

		var img = new Image();

		img.src = this.generator.imgSrc;


		if (img.width > img.height && img.width > 600 && img.height > 600) {


			var ratio = img.width / 600;

			if (img.height / ratio > 600) {

				img.width = 600;

				img.height = img.height / ratio;

			} else {


				var heightRatio = img.height / 600;

				img.height = 600;

				img.width = img.width / heightRatio;
			}

		} else if (img.height > img.width && img.height > 600 && img.width > 600) {

			var ratio = img.height / 600;

			if (img.width / ratio > 600) {

				img.height = 600;

				img.width = img.width / ratio;

			} else {

				var heightRatio = img.width / 600;

				img.width = 600;

				img.height = img.height / heightRatio;
			}

		} else if (img.width == img.height && img.height > 600) {

			img.height = 600;

			img.width = 600;
		} else {

			img.height = img.height;

			img.width = img.width;
		}


		img.height = img.height + 50;

		img.width = img.width + 50;

		var goLogo = new Image();

		goLogo.src = 'images/footer-logo.png';

		canvas.width = 600;

		canvas.height = 600;

		canvas.getContext('2d').drawImage(img, this.imageLeftPosition, this.imageTopPosition, img.width, img.height);

		canvas.getContext('2d').drawImage(goLogo, 515, 15, 70, 70);


		if ( this.generator.text.length > 1 ) {

			var context = canvas.getContext('2d');

			context.beginPath();
			context.rect(0, 530, 600, 70);
			context.fillStyle = 'rgba(0,119,35, .8)';
			context.fill();

			context.fillStyle = 'white';

			context.font = 'bold 40px Gotham_boldregular';

			context.textAlign = 'center';

			context.fillText('#GO' + this.generator.text, 300, 575);

		}


		this.generator.canvasImage = canvas.toDataURL("image/jpeg");

		jQuery('#drop-uploader').fadeOut();
		jQuery('#custom-man').fadeIn();
	}



	ngOnDestroy() {

		jQuery('body').removeClass('opacity');

	}

	ngAfterViewInit() {

       jQuery('#generatorCanvasImageContainer').draggable();
       
       console.log('blah blah'); 

		setTimeout(function() { jQuery('body').addClass('opacity') }, 800);



			new WOW().init();


			// home slider 
			jQuery('#carousel').slick({
				infinite: true,
				mobileFirst: true,
				slidesToShow: 3,
				slidesToScroll: 1,
				prevArrow: jQuery('.prev'),
				nextArrow: jQuery('.next'),
				responsive: [
					{
						breakpoint: 1600,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 1,
							infinite: true
						}
					},
					{
						breakpoint: 1000,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1,
							infinite: true
						}
					},
					{
						breakpoint: 735,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 413,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					},
					{
						breakpoint: 1,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 1
						}
					}
				]
			});


		if (this.router.get('location') === 'gear') {

			jQuery('body').animate({
				scrollTop: jQuery("#gear-container").offset().top
			}, 2000);

		}

		if (this.router.get('location') === 'generator') {

			jQuery('body').animate({

				scrollTop: jQuery("#generator-container").offset().top

			}, 2000);
		}

	}

}
