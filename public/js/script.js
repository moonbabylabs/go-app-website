jQuery(window).load(function() {
  jQuery('#preloader').delay(0).fadeOut(100);// will fade out the white DIV that covers the website.
}) 

$( document ).ready(function() {


  $('#jumbotron-play-btn').click(function(){
    $('.jumbotron-text-container').hide();
    $('#jumbotron-video').show();
  //  $('#close-video-btn').show();
    playVideo();
    $('#close-video-btn').click(function(){
      stopVideo();
      $('#jumbotron-video').hide();
    //  $('#close-video-btn').hide();
      $('.jumbotron-text-container').show();
    })
  });

  })
jQuery(document).ready(function() {
  "use strict";

    /*  Active Wow Js
    ============================================= */
    new WOW().init();
  // $('#preloader').delay(0).fadeOut(100);
// scrollTop
// $(window).scroll(function(){
//   if ($(this).scrollTop() > 50){
//     $('.navbar-default').addClass("navbar-fixed-top");
//   } else{
//     $('.navbar-default').removeClass("navbar-fixed-top");
//   }
// });

 


    /*  Slick Slider Active //testimonail
    ============================================= */
    $('.te-testimonial-wrap').slick({
        dots: false,
        infinite: true,
        nextArrow: 'span.next',
        prevArrow: 'span.prev',
        speed: 800,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    /*  Slick Slider Active //Business page
    ============================================= */
    $('.education-slider').slick({
        dots: false,
        infinite: true,
        nextArrow: 'span.next',
        prevArrow: 'span.prev',
        speed: 800,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    /*  Slick Slider Active //COmmunities page
    ============================================= */
    $('.bigslider-wrap').slick({
        dots: false,
        infinite: true,
        nextArrow: 'span.next',
        prevArrow: 'span.prev',
        speed: 800,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });


// home slider 
 $('#carousel').slick({
    infinite: true,
    mobileFirst: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
     responsive: [
        {
          breakpoint: 1600,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true
          }
        },
       {
         breakpoint: 1000,
         settings: {
           slidesToShow: 3,
           slidesToScroll: 1,
           infinite: true
         }
       },
      {
        breakpoint: 735,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 413,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
// Fancy box
$(".various").fancybox({
        maxWidth    : 800,
        maxHeight   : 600,
        fitToView   : false,
        width       : '70%',
        height      : '70%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });

// subscribe form
 // $('#subscribe-form').submit(function(){
 //            $.ajax({
 //              type : 'POST',
 //              url: 'mail-subscribe.php',
 //              data: {
 //                email: $('#exampleInputEmail2').val()
 //              },
 //              success: function(value){
 //                if(value == 1){
 //                  $('#subscribe-notification').addClass('success').css('color','yellow').text("Success to subscribe");
 //                }else {
 //                  $('#subscribe-notification').addClass('fail').css('color','red').text("Fail to subscribe");
 //                }
 //              }

 //            });
 //           return false;
 //        })

// Twitter API
 // !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");


}(jQuery)); //ready
