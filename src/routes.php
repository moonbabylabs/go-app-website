<?php
// Routes

use Slim\Views\PhpRenderer;


$container = $app->getContainer();
//$container['renderer'] = new PhpRenderer(getcwd() . "/templates");


/*
$app->get('/', function ($request, $response, $args) {

// Make a request to the GitHub API with a custom
// header of "X-Trvial-Header: Just as a demo".
$url = "https://sheetsu.com/apis/v1.0/62c711ee64f1";
$response = \Httpful\Request::get($url)
    ->send();

die(var_dump($response->body));

});
*/


$json = ['businessContent' => json_decode(file_get_contents('data/business-page.json'))];

$json['aboutContent'] = json_decode(file_get_contents('data/about-page.json'));

$json['communitiesContent'] = json_decode(file_get_contents('data/communities-page.json'));

$json['educationContent'] = json_decode(file_get_contents('data/education-page.json'));

$json['recreationContent'] = json_decode(file_get_contents('data/recreation-page.json'));

$json['newsContent'] = json_decode(file_get_contents('data/news-page.json'));

$json['goContent'] = json_decode(file_get_contents('data/go-page.json'));

$json['homeContent'] = json_decode(file_get_contents('data/home-page.json'));

$json['baseUrl'] = $_SERVER['HTTP_HOST'];

$app->get('/', function ($request, $response, $args) use ($json) {


	return $this->renderer->render( $response, 'index.phtml', $json );

});

$app->get('/email', function ($request, $response, $args) {
$headers = 'From: email-signup@gastonoutside.com';

mail('gastonoutside@gmail.com', 'new email signup', 'Email address: ' . $_GET['address'] , $headers);


mail('stephen@moonbabylabs.com', 'new email signup', 'Email address: ' . $_GET['address'] , $headers);

});

$app->get('/about', function ($request, $response, $args) use ($json) {

	return $this->renderer->render( $response, 'page.phtml', $json);

});

$app->get('/business', function ($request, $response, $args) use ($json) {


	return $this->renderer->render($response, 'page.phtml', $json);

});

$app->post('/download', function ($request, $response, $args) use ($json) {

	$currTime = time();
	$json = file_get_contents('php://input');

	$data = json_decode($json, true)['image'];

	list($type, $data) = explode(';', $data);
	list(, $data)      = explode(',', $data);
	$data = base64_decode($data);

	file_put_contents('generator/' . $currTime . '.jpg', $data);

	return 'generator/' . $currTime . '.jpg';


;
	
});


$app->get('/image', function ($request, $response, $args) {
header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename=go-logo.jpg');
	
	$contents = file_get_contents(getcwd() . '/' . $_GET['img']);

	return $contents;

});

$app->get('/communities', function ($request, $response, $args) use ($json) {

	$json = [];

	return $this->renderer->render($response, 'page.phtml', $json);

});

$app->get('/education', function ($request, $response, $args) use ($json) {

	$json = [];

	return $this->renderer->render($response, 'page.phtml', $json);

});

$app->get('/recreation', function ($request, $response, $args) use ($json) {

	$json = [];

	return $this->renderer->render($response, 'page.phtml', $json);

});

$app->get('/news', function ($request, $response, $args) use ($json) {

	$json = [];

	return $this->renderer->render($response, 'page.phtml', $json);

});

$app->get('/go', function ($request, $response, $args) use ($json) {

	$json = [];

	return $this->renderer->render($response, 'page.phtml', $json);

});


$app->get('/go-test', function ($request, $response, $args) use ($json) {

	$json = [];

	return $this->renderer->render($response, 'page.phtml', $json);

});


$app->get('/admin/business/update', function ($request, $response, $args) use ( $app ) {


	$json = ['content' => json_decode(file_get_contents('data/business-page.json'), true)];

	$json['formAction'] = '';

    // Render index view
    return $this->renderer->render($response, 'admin/forms.phtml', $json);


});


$app->post('/admin/business/update', function ($request, $response, $args) use ( $app ) {

	die(var_dump(file_put_contents('data/business-page.json', strtolower(json_encode( $request->getParams()  )))));


});
